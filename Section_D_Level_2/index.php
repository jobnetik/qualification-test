<?php 

require './config.php';

/**
 * Example of using Editor Class
 */

$Editor = Editor::getInstance();


/**
 * Create new 'Circle' and show it on the screen 
 */
$Editor->createShape('circle')->show();


/**
 * Create new 'Circle' change its color, resize it 
 * and print the image to the file located in the folder ./images/
 */
$Editor->createShape('circle')->setColor('#ff0000')->setSize(115, 115)->print();


/**
 * Create new 'Rectangle' change its color, resize it 
 * and print the image to the file located in the folder ./images/
 */
$Editor->createShape('rectangle')->setColor('#ff0000')->setSize(25, 125)->print();


/**
 * Create new 'Triangle' change its color, set its points 
 * and print the image to the file located in the folder ./images/
 */
$Editor->createShape('triangle')->setColor('#faff00')->setPoints(array(250, 100, 20, 180, 370, 380))->print();