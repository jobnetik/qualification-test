<?php 

/**
 * This is a factory class for creating Shapes instances
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
class ShapesFactory {

    /**
     * Create a new Spahe instance
     *
     * @param string $shapeName Name of the class to create
 	 * @return object[]
     */

	static function create($shapeName) {

		$shape = ucwords($shapeName);
		
		if(class_exists($shape)) {

			$Shape = new $shape;
			return $Shape;
		
		} else {
  			throw new Exception("Invalid shape.");
		}
	}
}