<?php 
/**
 * The class is responsible for creating Triangles
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
class Triangle extends Shape {

    /**
     * The array of Triangle points coordinates.
     *
     * @var array
     */
	private $points = array();


    /**
     * Prepare creating a new Triangle image and setting default coordinates for its points
     *
     * @return void
     */
	public function __construct() {
		parent::prepare();
		$this->points = array(150, 200, 100, 280, 200, 280);
	}


    /**
     * Create a new Triangle image
     *
     * @return void
     */
	private function create() {

	  	imagefilledpolygon ($this->image, $this->points, 3,  $this->color);
	}


    /**
     * Set a triangle points
     *
     * @param array $points  Array of points coordinates
 	 * @return object[]
     */
	public function setPoints($points = array()) {
		$this->points = (count($points) == 6) ? $points : $this->points;

		return $this;
	}
	

    /**
     * Display triangle image on the screen
     *
     * @return void
     */
	public function show() {

		$this->create();
		parent::show();
	}


    /**
     * Print triangle image to the file
     *
     * @param string $file  The file name.
     * @return void
     */
	public function print($file="") {

		$this->create();
		parent::print($file);
	}

}