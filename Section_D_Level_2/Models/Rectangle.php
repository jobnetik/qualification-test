<?php 
/**
 * The class is responsible for creating Rectangles
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
class Rectangle extends Shape {

    /**
     * Prepare creating a new Rectangle image.
     *
     * @return void
     */
	public function __construct() {
		parent::prepare();
	}

    /**
     * Create a new rectangle image
     *
     * @return void
     */
	private function create() {
		imagefilledrectangle($this->image, $this->coordX, $this->coordY, $this->width, $this->height, $this->color);
	}

    /**
     * Display rectangle image on the screen
     *
     * @return void
     */
	public function show() {

		$this->create();
		parent::show();
	}

    /**
     * Print triangle image to the file
     *
     * @param string $file  The file name.
     * @return void
     */
	public function print($file="") {

		$this->create();
		parent::print($file);
	}

}