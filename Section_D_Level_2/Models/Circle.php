<?php 
/**
 * The class is responsible for creating Circles
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
class Circle extends Shape {


    /**
     *  Prepare creating a new Circle image.
     *
     * @return void
     */
	public function __construct() {
		parent::prepare();
	}


    /**
     * Create a new circle image
     *
     * @return void
     */
	private function create() {
		imagefilledellipse($this->image, $this->coordX, $this->coordY, $this->width, $this->height, $this->color);
	}


    /**
     * Display circle image on the screen
     *
     * @return void
     */
	public function show() {

		$this->create();
		parent::show();
	}


    /**
     * Print triangle image to the file
     *
     * @param string $file  The file name.
     * @return void
     */
	public function print($file="") {

		$this->create();
		parent::print($file);
	}

}