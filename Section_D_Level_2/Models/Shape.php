<?php 

/**
 * This is an abstract class for creating Shapes
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
abstract class Shape implements iDrawable{


    /**
     * Image resource identifier
     *
     * @var resource
     */
	public $image;


	 /**
     * Shape filling color
     *
     * @var string
     */
	public $color;


	/**
     * Coordinate X of the shape
     *
     * @var integer
     */
	public $coordX = SHAPE_COORD_X;


	/**
     * Coordinate Y of the shape
     *
     * @var integer
     */
	public $coordY = SHAPE_COORD_Y;


	/**
     * Height of the shape
     *
     * @var integer
     */
	public $height = SHAPE_HEIGHT;


	/**
     * Width of the shape
     *
     * @var integer
     */
	public $width = SHAPE_WIDTH;


    /**
     * Convert color hex code to the RGB format
     *
     * @param string $color  The color hex code.
     * @return array
     */
	private function colorToRGB($color) {

		list($red, $green, $blue) = sscanf($color, "#%02x%02x%02x");
		return array('red' => $red, 'green' => $green, 'blue' => $blue);
	}


    /**
     * Set coordinates of the created shape
     * 
     * @param int $coordX  Shape X Coordinate
     * @param int $coordY  Shape Y Coordinate
     * @return object[]
     */
	public function setCoordinates($coordX, $coordY) {
		$this->coordX = $coordX;
		$this->coordY = $coordY;

		return $this;
	}

    /**
     * Set size of the created shape
     * 
     * @param int $height  Shape height
     * @param int width  Shape width
     * @return object[]
     */
	public function setSize($height, $width) {
		$this->height = $height;
		$this->width = $width;

		return $this;
	}


    /**
     * Set color of the created shape
     * 
     * @param string $color Shape filling color
     * @return object[]
     */
	public function setColor($color = "") {

		$color = empty($color) ? SHAPE_COLOR : $color;
		$rgb = $this->colorToRGB($color);
		$this->color = imagecolorallocate($this->image, $rgb['red'], $rgb['green'], $rgb['blue']);

		return $this;
	}


    /**
     * Prepare creating shape
     * 
     * @return void
     */
	protected function prepare() {

		$this->image = imagecreatetruecolor(CANVAS_WIDTH, CANVAS_HEIGHT);
		$this->setColor();
	}


    /**
     * Display shape image on the screen
     *
     * @return void
     */
	protected function show() {

		header('Content-type: image/png');
		imagepng($this->image);
		$this->destroy();
	}


    /**
     * Print shape image to the file
     *
     * @param string $file  The file name.
     * @return void
     */
	protected function print($file = "") {

		$file = empty($file) ? md5(microtime()) . '.png' : $file;

		imagepng($this->image, './images/'. $file);
		$this->destroy();
	}


    /**
     * Destroy shape image  
     * 
     * @return void
     */

	public function destroy() {
		imagedestroy($this->image);
	}

}