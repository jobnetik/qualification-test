<?php 
/**
 * The interface for drawable shapes
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
interface iDrawable {
	
	 /**
     * Set coordinates of the created shape
     * 
     * @param int $coordX  Shape X Coordinate
     * @param int $coordY  Shape Y Coordinate
     * @return object[]
     */
	public function setCoordinates($coordX, $coordY);


	 /**
     * Set size of the created shape
     * 
     * @param int $height  Shape height
     * @param int width  Shape width
     * @return object[]
     */
	public function setSize($height, $width);


    /**
     * Set color of the created shape
     * 
     * @param string $color Shape filling color
     * @return object[]
     */
	public function setColor($color);
}