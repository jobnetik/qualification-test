<?php 
/**
 * This is a singelton class responsible for creating Editor instance
 *
 * @author     Eugene Dvoynishnikov <eugene.dvoir@fdata.org>
 */
class Editor {

    /**
     * Editor instance
     *
     * @var object
     */
	private static $editor = null;


    /**
     * Prevent creating new Editor instances
     *
     * @return bool
     */
	private function __construct() {
		return;
	}


    /**
     * Prevent cloning Editor instances
     *
     * @return bool
     */
	private function __clone() {
		return;
	}


    /**
     * Create or get current Editor instance
     *
     * @return object[]
     */
	public static function getInstance() {
		if(self::$editor == null) {
			self::$editor = new Editor();
		}
		return self::$editor;
	}
	
    /**
     * Create new Shape
     * 
     * @param string $shapeName  Name of the Shape.
     * @return object[]
     */
	public function createShape($shapeName) {
		return ShapesFactory::create($shapeName);
	}

}