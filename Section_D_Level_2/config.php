<?php 

require './autoload.php';	

define("CANVAS_HEIGHT", 800);
define("CANVAS_WIDTH", 800);

define("SHAPE_COLOR", "#ffffff");
define("SHAPE_COORD_X", 300);
define("SHAPE_COORD_Y", 300);
define("SHAPE_HEIGHT", 100);
define("SHAPE_WIDTH", 100);