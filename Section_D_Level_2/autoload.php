<?php 

 /**
  * Function for autoloading classes
  *
  * @param string $class_name Class name to load
  *
  * @return void
  */
	spl_autoload_register(function ($class_name) {
	    include './Models/' . $class_name . '.php';
	});