<?php 

/**
 * Functions finds minimum string in the array of string
 * 
 * @param array $words  Array of strings.
 * @return string  Minimum string in the given array
 */
function getMinimumInStringsArray($words = array()) {
    if(empty($words)) {
        return false;
    }

    $minWord = $words[0];

    foreach ($words as $word) {
        $minWord = (strcmp($word, $minWord) < 0) ? $word : $minWord;
    }

    return $minWord;
}


// Test

$words = array('my','name','is','john','doe', 'dot', 'dae');
echo "minimum: " . getMinimumInStringsArray($words);
