
//**************************
// Advanced level solution:

SELECT t1.uid, t1.dt, t1.s
FROM t AS t1
LEFT OUTER JOIN t AS t2
     ON t1.uid = t2.uid
     AND(t1.dt < t2.dt)
     
WHERE t2.uid IS NULL




//****************************
// Intermediate level solution

SELECT uid, dt, s
FROM t AS t1
WHERE dt IN (SELECT MAX(dt) FROM t AS t2 WHERE t1.uid = t2.uid);