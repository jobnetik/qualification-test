<?php 

/**
 * Functions finds summ of the tree elements
 * 
 * @param array consists of numbers and inner arrays 
 * @return integer  Sum of all elements
 */
function getSumOfArrayElements($treeElements = array()) {

    $sum = 0;

    foreach ($treeElements as $element) {
        $sum += (is_array($element)) ? getSumOfArrayElements($element) : $element;
    }

    return $sum;
}


// Test
$numbers = array( 10, 
	array(20,30), 
	array(-5, 
		array(6, 7, 
			array(8, 
				array(9), 10, 
				array( 
					array(), 
					array()), 
				11),
			-15,
			-16
		)
	)
);

echo "\nsum: " . getSumOfArrayElements($numbers);